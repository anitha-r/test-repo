import numpy as np
import pandas as pd
from os import path
from PIL import Image
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib.pyplot as plt
%matplotlib inline

#import warnings

df = pd.read_csv("C:\Anu\Learning\Python\wordcloud\winemag-data-130k-v2.csv", index_col=0)
df.head()

print("There are {} observations and {} features in this dataset. \n".format(df.shape[0],df.shape[1]))

print ("There are {} types of wine in this dataset such as {} ... \n".format(len(df.variety.unique()), ", ".join(df.variety.unique()[0:5])))

print("There are {} countries producing wine in this dataset such as {}... \n".format(len(df.country.unique()),", ".join(df.country.unique()[0:5])))

df[["country","description","points"]].head()

#group by country
country = df.groupby("country")

#summary statistic of all countries
country.describe().head()

#this selects the top 5 highest average points among all 4 countries
country.mean().sort_values(by="points",ascending=False).head()

#plot the number of wines by country using the plot method of Pandas DataFrame and Matplotlib
plt.figure(figsize=(15,10))
#country.size().sort_values(ascending=False).plot.bar()
country.max().sort_values(by="points",ascending=False)["points"].plot.bar()
plt.xticks(rotation=50)
plt.xlabel("Country of Origin")
#plt.ylabel("Number of Wines")
plt.ylabel("Highest point of Wines")
plt.show()

#setting up basic wordcloud
#start with one review:
#text = df.description[0]

#create and generate a word cloud image:
#wordcloud = WordCloud().generate(text)

#lower max_font size , change the maximum number of word and lighten the background
#wordcloud  = WordCloud(max_font_size=50, max_words=100, background_color="white").generate(text)

#display the generated image:
#plt.figure()
#plt.imshow(wordcloud, interpolation='bilinear')
#plt.axis("off")
#plt.show()

#save the image in the img folder:
wordcloud.to_file("first_review.png")

#combine all wine reviews into one big text and create a big fat cloud 
#to see which characteristic is most common
text=" ".join(review for review in df.description)
print("There are {} words in the combination of all review".format(len(text)))

#create stopword list
stopwords = set(STOPWORDS)
stopwords.update(["drink","now","wine","flavor","flavors"])

#generate a word cloud image
wordcloud = WordCloud(stopwords=stopwords,background_color='white').generate(text)

#Display the generated image:
#the matplotlib way
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")
plt.show()
wordcloud.to_file("stopword_cloud.png")