#!/usr/bin/env python
# coding: utf-8

#there are 2 files
#file1 contains raw accelerometer data with the following format
#timestamp, UTC time, accuracy, x,y,z
#x,y,z correspond to measurements of linear acceleration along each of the 3 orthogonal axes
#file2 contains activity labels 
#activities have been numbered as follows:
#standing : 1, walking : 2, stairsdown : 3, stairsup : 4
 


#import modules to current namespace
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import scipy.stats

import sklearn.linear_model
import sklearn.neighbors
import sklearn.svm

import time
import datetime as dt
import math


###read the files to a dataframe and print summary###
train_series = pd.read_csv("train_time_series.csv", index_col=0)
train_label = pd.read_csv("train_labels.csv", index_col=0)

test_series = pd.read_csv("test_time_series.csv", index_col=0)
test_label = pd.read_csv("test_labels.csv", index_col=0)

#train_series.info())
print(train_label.info())
test_series.head()


###time series visualization
plt.figure(figsize=(20, 8))
plt.plot(train_series['timestamp'], train_series['x'], 'b-', label = 'x variable')
plt.plot(train_series['timestamp'], train_series['y'], 'r-', label = 'y variable')
plt.plot(train_series['timestamp'], train_series['z'], 'g-', label = 'y variable')
plt.xlabel('Timestamp'); plt.ylabel('Acceleration'); plt.title('Time series for accelerometer signals')
plt.legend();


#calculate vector magnitude based on the three axes
def vector_magnitude(activity):
    x2=activity['x'] ** 2
    y2=activity['y'] ** 2
    z2=activity['z'] ** 2
    msum = x2 + y2 + z2
    magnitude = msum.apply(lambda x: math.sqrt(x))
    return magnitude


# In[13]:


train_series['mag'] = vector_magnitude(train_series)
#train_series.head()

test_series['mag'] = vector_magnitude(test_series)
test_series.head()


# In[10]:


plt.figure(figsize=(20, 8))
plt.plot(train_series['timestamp'], train_series['mag'], 'b-', label = 'Magnitude variable')
plt.xlabel('Timestamp'); plt.ylabel('Acceleration'); plt.title('Time series for accelerometer signals')
plt.legend();


# In[35]:


columns=['timestamp','x','y','z','mag','label']

train_merge = pd.merge(train_label, train_series)

standing = train_merge[columns][train_merge['label']==1]
walking = train_merge[columns][train_merge['label']==2]
stairsdown = train_merge[columns][train_merge['label']==3]
stairsup = train_merge[columns][train_merge['label']==4]
standing


# In[37]:


def plot_axis(ax, x, y, title):
    ax.plot(x, y)
    ax.set_title(title)
    ax.xaxis.set_visible(False)
    ax.set_ylim([min(y) - np.std(y), max(y) + np.std(y)])
    ax.set_xlim([min(x), max(x)])
    ax.grid(True)
    
def plot_activity(activities, titles):
    fig, axs = plt.subplots(nrows=len(activities), figsize=(10, 8))
    for i in range(0, len(activities)):
        plot_axis(axs[i], activities[i]['timestamp'], activities[i]['mag'], titles[i])
    plt.subplots_adjust(hspace=0.2)
    plt.show()

plot_activity([standing, walking, stairsdown, stairsup],
              ['Standing', 'Walking', 'Stairs down', 'Stairs sup'])

 
    


# In[82]:


fig,ax1 = plt.subplots(nrows=4, figsize=(20, 8))
ax=ax1[0]
x=standing['timestamp']
y= standing['mag']
title= 'Standing'

ax.plot(x, y)
ax.set_title(title)
ax.set_ylim([min(y) , max(y)])
ax.set_xlim([min(x), max(x)])
ax.grid(True)
    


ax=ax1[1]
x=walking['timestamp']
y= walking['mag']
title= 'Walking'

ax.plot(x, y,'r')
ax.set_title(title)
ax.set_ylim([min(y) , max(y)])
ax.set_xlim([min(x), max(x)])
ax.grid(True)

ax=ax1[2]
x=stairsdown['timestamp']
y= stairsdown['mag']
title= 'Stairs Down'

ax.plot(x, y,'g')
ax.set_title(title)
ax.set_ylim([min(y) , max(y)])
ax.set_xlim([min(x), max(x)])
ax.grid(True)

ax=ax1[3]
x=stairsup['timestamp']
y= stairsup['mag']
title= 'Stairs Up'

ax.plot(x, y,'y')
ax.set_title(title)
ax.set_ylim([min(y) , max(y)])
ax.set_xlim([min(x), max(x)])
ax.grid(True)
    
plt.subplots_adjust(hspace=0.2)
plt.show()







